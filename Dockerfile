FROM node:8.5.0

RUN apt-get update \
 && apt-get install bzip2 g++ libfontconfig1 libfreetype6 make python-minimal -y \
 && echo "Europe/Berlin" > /etc/timezone && dpkg-reconfigure -f noninteractive tzdata

COPY package.json package.json

# workaround for this issue: https://github.com/yarnpkg/yarn/issues/2266
RUN yarn global add node-gyp
# end workaround

COPY . .

RUN yarn --production \
 && yarn build
